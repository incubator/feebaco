#!/bin/bash

DBdir=/home/postgresql-13
DB=feebaco
docker run \
  --name postgres-13-${DB} \
  --restart always \
  -v ${DBdir}/${DB}:/var/lib/postgresql/data \
  -e POSTGRES_DB=${DB} \
  -e POSTGRES_PASSWORD=whatever \
  -p 45432:5432 \
  -d postgres:13
