package org.chorem.feebaco.backend.connectors;

import org.chorem.feebaco.backend.rest.dtos.FeedbackDto;
import org.chorem.feebaco.backend.rest.dtos.ImmutableFeedbackDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;

import static com.google.common.collect.ImmutableMap.of;
import static org.junit.jupiter.api.Assertions.assertEquals;

class TemplateHelperTest {

    protected static TemplateHelper templates;

    @BeforeAll
    public static void initTemplateHelper() {
        templates = new TemplateHelper();
    }

    @Test
    public void testSimple() {
        assertEquals("Youcou ",     templates.render("Youcou {{who}}", of()));
        assertEquals("Youcou Mig",  templates.render("Youcou {{who}}", of("who", "Mig")));
        assertEquals("Youcou lélé", templates.render("Youcou {{who}}", of("who", "lélé")));
    }

    @Test
    public void testConditional() {
        assertEquals("Youcou",         templates.render("Youcou{{#who}} someone{{/who}}", of()));
        assertEquals("Youcou someone", templates.render("Youcou{{#who}} someone{{/who}}", of("who", "Mig")));
        assertEquals("Youcou Mig",     templates.render("Youcou{{#who}} {{who}}{{/who}}", of("who", "Mig")));
        assertEquals("Youcou Pouic",   templates.render("Youcou{{#who}} {{name}}{{/who}}", of("who", of("name", "Pouic"))));
    }

    @Test
    public void testAbsent() {
        assertEquals("Youcou someone", templates.render("Youcou{{^who}} someone{{/who}}", of()));
        assertEquals("Youcou",         templates.render("Youcou{{^who}} someone{{/who}}", of("who", "Mig")));
    }

    @Test
    public void testConditionalOptional() {
        assertEquals("Youcou someone", templates.render("Youcou{{#who}} someone{{/who}}{{^who}} noone{{/who}}", of("who", Optional.of("Mig"))));
        assertEquals("Youcou noone",   templates.render("Youcou{{#who}} someone{{/who}}{{^who}} noone{{/who}}", of("who", Optional.empty())));
        assertEquals("Youcou Pouic",   templates.render("Youcou{{#who}} {{name}}{{/who}}{{^who}} noone{{/who}}", of("who", of("name", "Pouic"))));
        assertEquals("Youcou noone",   templates.render("Youcou{{#who}} {{name}}{{/who}}{{^who}} noone{{/who}}", of("who", Optional.empty())));
    }

    @Test
    public void testLambda() {
        Function<String, String> revert = (s) -> {
            StringBuilder sb = new StringBuilder();
            for (int i=1; i<=s.length(); i++) {
                sb.append(s.charAt(s.length() - i));
            }
            String result = sb.toString();
            return result;
        };
        assertEquals("Youcou odoR", templates.render("Youcou {{#revert}}Rodo{{/revert}}", of("revert", revert)));
    }

    @Test
    public void testMapForEach() {
        Map<String, String> whoMap = of("name", "Pouic", "level", "MAXIMUM");
        assertEquals("Youcou Pouic: MAXIMUM", templates.render("Youcou {{#who}}{{name}}: {{level}}{{/who}}", of("who", whoMap)));
        assertEquals("Youcou name level ",              templates.render("Youcou {{#whoEntries}}{{key}} {{/whoEntries}}", of("whoEntries", whoMap.entrySet())));
        assertEquals("Youcou Pouic MAXIMUM ",              templates.render("Youcou {{#whoEntries}}{{value}} {{/whoEntries}}", of("whoEntries", whoMap.entrySet())));
        assertEquals("Youcou ",              templates.render("Youcou {{#whoEntries}}{{value}} {{/whoEntries}}", of()));
    }

    @Test
    public void testEmailTemplate() {
        FeedbackDto feedback = ImmutableFeedbackDto.builder()
                .feedbackId(UUID.randomUUID())
                .createdOn(ZonedDateTime.now())
                .title("#saynul")
                .category("Dunno")
                .putExtra("key1", "someValue")
                .putExtra("key2", "blabla")
                .description("Une petite descricri")
                .os("nunux")
                .build();
        Map<String, Object> templateParameters = of(
                "f", feedback,
                "screenshot", true,
                "extraEntries", feedback.getExtra().entrySet()
        );
        String body = templates.renderResource("templates/email.mustache", templateParameters);
        Assertions.assertTrue(body.contains("<title>Nouveau feedback : #saynul</title>"));
        Assertions.assertTrue(body.contains("catégorie 'Dunno'"));
        Assertions.assertTrue(body.contains("<li>L'utilisateur a fournit une capture d'écran (cf PJ)</li>"));
        Assertions.assertTrue(body.contains("Une petite descricri"));
        Assertions.assertTrue(body.contains("<li>OS : nunux</li>"));
        Assertions.assertTrue(body.contains("<li>key1 : someValue</li>"));
        Assertions.assertTrue(body.contains("<li>key2 : blabla</li>"));
    }

}