package org.chorem.feebaco.backend.rest;

import io.quarkus.test.junit.QuarkusTest;
import org.chorem.feebaco.backend.database.entities.ActionType;
import org.chorem.feebaco.backend.rest.dtos.ActionDto;
import org.chorem.feebaco.backend.rest.dtos.ImmutableClientInputDto;
import org.chorem.feebaco.backend.rest.dtos.ImmutableProjectInputDto;
import org.chorem.feebaco.backend.rest.dtos.ImmutableStrategyInputDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.testcontainers.shaded.com.google.common.collect.ImmutableMap;

import javax.ws.rs.core.MediaType;
import java.util.Optional;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.withArgs;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.notNullValue;

@QuarkusTest
class ProjectsResourceTest {

    @Test
    public void testCreateProject() {

        String clientId = getClientId();

        int countBefore = countProjectsByClientId(clientId);

        // On créé l'objet en entrée
        String projectName = "lkŝdjflmkùfds";
        ImmutableProjectInputDto input = ImmutableProjectInputDto.builder()
                .name(projectName)
                .addCategory("Azerty")
                .addCategory("WHATEVER")
                .build();

        // ... et on fait l'appel de création
        String projectId =
                given()
                        .body(input)
                        .contentType(MediaType.APPLICATION_JSON)
                        .post("/api/v1/clients/{clientId}/projects", clientId)
                    .then()
                        .statusCode(200)
                        // response looks like :
                        //     {"name":"taiste","categories":["WHATEVER","Azerty"],"projectId":"ca137880-15eb-4408-ad80-99f6f4634729","accessKey":"bfFHSVCjW7dLKJZR4pgJwc2oCg5YZQyK"}
                        .body("name", equalTo(projectName))
                        .body("categories", hasItems("Azerty", "WHATEVER"))
                        .body("projectId", notNullValue())
                        .body("accessKey", notNullValue())
                    .extract()
                        .path("projectId");

        int countAfter = countProjectsByClientId(clientId);
        Assertions.assertEquals(countBefore + 1, countAfter);

        // On refait un appel pour charger les projets du client et on vérifie qu'on retrouve bien le projet avec le bon nom
        given()
                .get("/api/v1/clients/{clientId}/projects", clientId)
            .then()
                .statusCode(200)
                // response looks like :
                //    {"elements":[{"name":"lkŝdjflmkùfds","categories":["WHATEVER","Azerty"],"projectId":"aebc53e1-531d-4fcb-a8af-bfccef22545f","accessKey":"CCT759W1y9ZYWpLfjuaSjysTHozssfc8"}],"count":1,"currentPage":{"pageNumber":0,"pageSize":-1,"orderClauses":[]}}
                .body("elements.find { it.projectId == '%s' }.name", withArgs(projectId), equalTo(projectName));

    }

    @Test
    public void testCreateProjectDuplicate() {

        String clientId = getClientId();

        // On créé l'objet en entrée
        String projectName = "testCreateProjectDuplicate";
        ImmutableProjectInputDto input = ImmutableProjectInputDto.builder()
                .name(projectName)
                .addCategory("Azerty")
                .addCategory("WHATEVER")
                .build();

        // on créé une première fois le projet
        given()
                .body(input)
                .contentType(MediaType.APPLICATION_JSON)
                .post("/api/v1/clients/{clientId}/projects", clientId)
            .then()
                .statusCode(200);

        // ... et on recommence -> le duplicat doit être détecté
        given()
                .body(input)
                .contentType(MediaType.APPLICATION_JSON)
                .post("/api/v1/clients/{clientId}/projects", clientId)
            .then()
                .statusCode(400)
                .body("error", equalTo("A project already exists with this name"));

    }

    @Test
    public void testCreateProjectInputErrors() {

        String clientId = getClientId();

        {
            // Pas de nom / nom vide
            ImmutableProjectInputDto input = ImmutableProjectInputDto.builder()
                    .name("")
                    .addCategory("WHATEVER")
                    .build();

            given()
                    .body(input)
                    .contentType(MediaType.APPLICATION_JSON)
                    .post("/api/v1/clients/{clientId}/projects", clientId)
                .then()
                    .statusCode(400)
                    .body("error", equalTo("Project's name is mandatory"));
        }

        {
            // Pas de catégorie
            ImmutableProjectInputDto input = ImmutableProjectInputDto.builder()
                    .name("testCreateProjectInputErrors")
                    .build();

            given()
                    .body(input)
                    .contentType(MediaType.APPLICATION_JSON)
                    .post("/api/v1/clients/{clientId}/projects", clientId)
                .then()
                    .statusCode(400)
                    .body("error", equalTo("Project's feedback's categories are mandatory"));
        }

        {
            // Catégorie vide
            ImmutableProjectInputDto input = ImmutableProjectInputDto.builder()
                    .name("testCreateProjectInputErrors")
                    .addCategory("")
                    .addCategory("WHATEVER")
                    .build();

            given()
                    .body(input)
                    .contentType(MediaType.APPLICATION_JSON)
                    .post("/api/v1/clients/{clientId}/projects", clientId)
                .then()
                    .statusCode(400)
                    .body("error", equalTo("Empty category is not allowed"));
        }

    }

    @Test
    public void testCreateProjectStrategies() {

        String clientId = getClientId();

        // On créé l'objet en entrée
        ImmutableProjectInputDto input = ImmutableProjectInputDto.builder()
                .name("sdfslkmsjdf")
                .addCategory("WHATEVER")
                .build();

        // ... et on fait l'appel de création
        String projectId =
                given()
                        .body(input)
                        .contentType(MediaType.APPLICATION_JSON)
                        .post("/api/v1/clients/{clientId}/projects", clientId)
                    .then()
                        .statusCode(200)
                    .extract()
                        .path("projectId");

        // On charge les stratégies et on vérifie la présence de la stratégie par défaut
        String defaultStrategyId = given()
                .get("/api/v1/projects/{projectId}/strategies", projectId)
            .then()
                .statusCode(200)
                // response looks like :
                //    [{"expression":"true","actions":[{"type":"STORE","template":null,"parameters":null}],"strategyId":"a1013ef1-47f2-4514-bbe6-747de77a4c02"}]
                .body("[0].expression", equalTo("true"))
                .body("[0].actions[0].type", equalTo("STORE"))
            .extract()
                .path("[0].strategyId");

        // On créé une nouvelle stratégie de type EMAIL avec une condition sur le titre
        ImmutableStrategyInputDto strategy = ImmutableStrategyInputDto.builder()
                .expression("f.title.length < 3")
                .addAction(new ActionDto(ActionType.EMAIL, "azertyuiop", ImmutableMap.of("to", "someValue@toto.fr")))
                .build();

        given()
                .body(strategy)
                .contentType(MediaType.APPLICATION_JSON)
                .post("/api/v1/projects/{projectId}/strategies", projectId)
            .then()
                .statusCode(200)
                // response looks like :
                //    {"expression":"f.title.length < 3","actions":[{"type":"EMAIL","template":"azertyuiop","parameters":{"someKey":"someValue"}}],"strategyId":"cc3b6fbc-ef8a-4df3-a21e-f825a010d4cb"}
                .body("expression", equalTo("f.title.length < 3"))
                .body("actions[0].type", equalTo("EMAIL"))
                .body("actions[0].template", equalTo("azertyuiop"))
                .body("actions[0].parameters.to", equalTo("someValue@toto.fr"));

        // On charge les stratégies et on vérifie la présence de la nouvelle stratégie
        given()
                .get("/api/v1/projects/{projectId}/strategies", projectId)
            .then()
                .statusCode(200)
                // response looks like :
                //    [{"expression":"true","actions":[{"type":"STORE","template":null,"parameters":null}],"strategyId":"96fe44c7-0fda-41c1-ba3d-d2599ca9135b"},{"expression":"f.title.length < 3","actions":[{"type":"EMAIL","template":null,"parameters":null}],"strategyId":"d9344ecc-6521-4e90-a719-ed649f8427af"}]
                .body("[0].expression", equalTo("true"))
                .body("[0].actions[0].type", equalTo("STORE"))
                .body("[1].expression", equalTo("f.title.length < 3"))
                .body("[1].actions[0].type", equalTo("EMAIL"))
                .body("[1].actions[0].template", equalTo("azertyuiop"))
                .body("[1].actions[0].parameters.to", equalTo("someValue@toto.fr"));

        // On supprime la stratégie par défaut
        given()
                .delete("/api/v1/projects/{projectId}/strategies/{strategyId}", projectId, defaultStrategyId)
            .then()
                .statusCode(204);

        // On charge les stratégies et on vérifie que la stratégie par défaut a disparu
        given()
                .get("/api/v1/projects/{projectId}/strategies", projectId)
            .then()
                .statusCode(200)
                // response looks like :
                //    [{"expression":"true","actions":[{"type":"STORE","template":null,"parameters":null}],"strategyId":"96fe44c7-0fda-41c1-ba3d-d2599ca9135b"},{"expression":"f.title.length < 3","actions":[{"type":"EMAIL","template":null,"parameters":null}],"strategyId":"d9344ecc-6521-4e90-a719-ed649f8427af"}]
                .body("[0].expression", equalTo("f.title.length < 3"))
                .body("[0].actions[0].type", equalTo("EMAIL"));

    }

    @Test
    public void testCreateProjectStrategiesErrors() {

        String clientId = getClientId();

        // On créé l'objet en entrée
        ImmutableProjectInputDto input = ImmutableProjectInputDto.builder()
                .name("testCreateProjectStrategiesErrors")
                .addCategory("WHATEVER")
                .build();

        // ... et on fait l'appel de création
        String projectId =
                given()
                        .body(input)
                        .contentType(MediaType.APPLICATION_JSON)
                        .post("/api/v1/clients/{clientId}/projects", clientId)
                    .then()
                        .statusCode(200)
                    .extract()
                        .path("projectId");

        {
            // On créé une nouvelle stratégie de type EMAIL avec une condition sur le titre
            ImmutableStrategyInputDto strategy = ImmutableStrategyInputDto.builder()
                    .expression("f.title.length < 3")
                    .addAction(new ActionDto(ActionType.EMAIL, "azertyuiop", null))
                    .build();

            given()
                    .body(strategy)
                    .contentType(MediaType.APPLICATION_JSON)
                    .post("/api/v1/projects/{projectId}/strategies", projectId)
                .then()
                    .statusCode(400)
                    .body("error", equalTo("Missing parameters"));
        }

        {
            // On créé une nouvelle stratégie de type EMAIL avec une condition sur le titre
            ImmutableStrategyInputDto strategy = ImmutableStrategyInputDto.builder()
                    .expression("f.title.length < 3")
                    .addAction(new ActionDto(ActionType.EMAIL, "azertyuiop", ImmutableMap.of("to", "someValue")))
                    .build();

            given()
                    .body(strategy)
                    .contentType(MediaType.APPLICATION_JSON)
                    .post("/api/v1/projects/{projectId}/strategies", projectId)
                .then()
                    .statusCode(400)
                    .body("error", equalTo("Invalid email address: someValue"));
        }

        {
            // On créé une nouvelle stratégie de type EMAIL avec une condition sur le titre
            ImmutableStrategyInputDto strategy = ImmutableStrategyInputDto.builder()
                    .expression("f.title.length < 3")
                    .addAction(new ActionDto(ActionType.EMAIL, "azertyuiop", ImmutableMap.of("to", ",")))
                    .build();

            given()
                    .body(strategy)
                    .contentType(MediaType.APPLICATION_JSON)
                    .post("/api/v1/projects/{projectId}/strategies", projectId)
                .then()
                    .statusCode(400)
                    .body("error", equalTo("At least one valid 'to' address is necessary"));
        }

    }

    protected int countProjectsByClientId(String clientId) {
        int count = given()
                .get("/api/v1/clients/{clientId}/projects", clientId)
            .then()
                .statusCode(200)
            .extract()
                .path("count");

        return count;
    }

    protected Optional<String> findClientId(String clientName) {
        String clientId = given()
                .get("/api/v1/clients")
            .then()
                .statusCode(200)
            .extract()
                .path("elements.find { it.name == '%s' }.clientId", clientName);
        Optional<String> result = Optional.ofNullable(clientId);
        return result;
    }

    protected String createClient(String clientName) {
        ImmutableClientInputDto input = ImmutableClientInputDto.builder().name(clientName).build();
        String clientId = given()
                .body(input)
                .contentType(MediaType.APPLICATION_JSON)
                .post("/api/v1/clients")
            .then()
                .statusCode(200)
            .extract()
                .path("clientId");
        return clientId;
    }

    protected String getClientId() {
        String clientName = "ProjectsResourceTestClient";
        String clientId = findClientId(clientName).orElseGet(() -> createClient(clientName));
        return clientId;
    }

}
