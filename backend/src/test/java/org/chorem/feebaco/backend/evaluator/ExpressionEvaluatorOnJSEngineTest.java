package org.chorem.feebaco.backend.evaluator;

import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.RandomStringUtils;
import org.chorem.feebaco.backend.rest.dtos.FeedbackDto;
import org.chorem.feebaco.backend.rest.dtos.ImmutableFeedbackDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Map;
import java.util.UUID;

import static org.chorem.feebaco.backend.evaluator.Constants.DEFAULT_STRATEGY_EXPRESSION;

public class ExpressionEvaluatorOnJSEngineTest {

    protected ExpressionEvaluatorOnJSEngine wrap(String expression) {
        ExpressionEvaluatorOnJSEngine result = new ExpressionEvaluatorOnJSEngine(() -> expression);
        return result;
    }

    protected ImmutableFeedbackDto.Builder newFeedbackBuilder() {
        ImmutableFeedbackDto.Builder result = ImmutableFeedbackDto.builder()
                .feedbackId(UUID.randomUUID())
                .createdOn(ZonedDateTime.now())
                .title(RandomStringUtils.random(12))
                .category(RandomStringUtils.random(12));
        return result;
    }

    protected ImmutableFeedbackDto newTitleFeedback(String title) {
        ImmutableFeedbackDto result = newFeedbackBuilder()
                .title(title)
                .build();
        return result;
    }

    protected ImmutableFeedbackDto newCategoryFeedback(String category) {
        ImmutableFeedbackDto result = newFeedbackBuilder()
                .category(category)
                .build();
        return result;
    }

    @Test
    public void testDefaultStrategyExpression() {
        ExpressionEvaluatorOnJSEngine defaultExpression = wrap(DEFAULT_STRATEGY_EXPRESSION);
        Assertions.assertTrue(defaultExpression.test(null));
        FeedbackDto feedback = newCategoryFeedback("Whatever");
        Assertions.assertTrue(defaultExpression.test(feedback));
    }

    @Test
    public void testAlwaysTrue() {
        ExpressionEvaluatorOnJSEngine defaultExpression = wrap("true");
        Assertions.assertTrue(defaultExpression.test(null));
        FeedbackDto feedback = newCategoryFeedback("Whatever");
        Assertions.assertTrue(defaultExpression.test(feedback));
    }

    @Test
    public void testAlwaysFalse() {
        ExpressionEvaluatorOnJSEngine defaultExpression = wrap("false");
        Assertions.assertFalse(defaultExpression.test(null));
        FeedbackDto feedback = newCategoryFeedback("Whatever");
        Assertions.assertFalse(defaultExpression.test(feedback));
    }

    @Test
    public void testSimpleExpression() {
        ExpressionEvaluatorOnJSEngine defaultExpression = wrap("f.category == 'BUG'");

        {
            FeedbackDto feedback = newCategoryFeedback("BUG");
            Assertions.assertTrue(defaultExpression.test(feedback));
        }

        {
            FeedbackDto feedback = newCategoryFeedback("SUGGESTION");
            Assertions.assertFalse(defaultExpression.test(feedback));
        }
    }

    @Test
    public void testContains() {
        ExpressionEvaluatorOnJSEngine defaultExpression = wrap("f.title.indexOf('bateau') != -1");

        {
            FeedbackDto feedback = newTitleFeedback("Le bateau coule !");
            Assertions.assertTrue(defaultExpression.test(feedback));
        }

        {
            FeedbackDto feedback = newTitleFeedback("La barque coule !");
            Assertions.assertFalse(defaultExpression.test(feedback));
        }
    }

    @Test
    public void testMissingValue() {
        ExpressionEvaluatorOnJSEngine evaluator1 = wrap("!f.frontendVersion.present");
        ExpressionEvaluatorOnJSEngine evaluator2 = wrap("f.frontendVersion.empty");

        {
            FeedbackDto feedback = newFeedbackBuilder().build();
            Assertions.assertTrue(evaluator1.test(feedback));
            Assertions.assertTrue(evaluator2.test(feedback));
        }

        {
            FeedbackDto feedback = newFeedbackBuilder().frontendVersion("azerty").build();
            Assertions.assertFalse(evaluator1.test(feedback));
            Assertions.assertFalse(evaluator2.test(feedback));
        }
    }

    @Test
    public void testMissingExtraValue() {
        ExpressionEvaluatorOnJSEngine evaluator = wrap("!f.extra.whatever");

        {
            FeedbackDto feedback = newFeedbackBuilder().build();
            Assertions.assertTrue(evaluator.test(feedback));
        }

        {
            FeedbackDto feedback = newFeedbackBuilder().putExtra("whatever", "azerty").build();
            Assertions.assertFalse(evaluator.test(feedback));
        }
    }

    @Test
    public void testHasValue() {
        ExpressionEvaluatorOnJSEngine evaluator1 = wrap("f.frontendVersion.present");
        ExpressionEvaluatorOnJSEngine evaluator2 = wrap("!f.frontendVersion.empty");

        {
            FeedbackDto feedback = newFeedbackBuilder().build();
            Assertions.assertFalse(evaluator1.test(feedback));
            Assertions.assertFalse(evaluator2.test(feedback));
        }

        {
            FeedbackDto feedback = newFeedbackBuilder().frontendVersion("azerty").build();
            Assertions.assertTrue(evaluator1.test(feedback));
            Assertions.assertTrue(evaluator2.test(feedback));
        }
    }

    @Test
    public void testHasExtraValue() {
        ExpressionEvaluatorOnJSEngine evaluator = wrap("!!f.extra.whatever");

        {
            FeedbackDto feedback = newFeedbackBuilder().build();
            Assertions.assertFalse(evaluator.test(feedback));
        }

        {
            FeedbackDto feedback = newFeedbackBuilder().putExtra("whatever", "azerty").build();
            Assertions.assertTrue(evaluator.test(feedback));
        }
    }

    @Test
    public void testTransitive() {
        ExpressionEvaluatorOnJSEngine evaluator = wrap("f.extra.monster == 'Lyche'");

        Map<String, Object> extraData = ImmutableMap.of(
                "monster", "Lyche"
        );
        FeedbackDto feedback = newFeedbackBuilder().extra(extraData).build();
        Assertions.assertTrue(evaluator.test(feedback));
    }

    @Test
    @Disabled // Le test échoue car f.extra n'est pas reconnu comme un objet
    public void testObjectKeys() {
        ExpressionEvaluatorOnJSEngine evaluator = wrap("Object.keys(f.extra).length == 3");

        Map<String, Object> extraData = ImmutableMap.of(
                "name", "Passoa",
                "race", "Labrador",
                "age", 9
        );
        FeedbackDto feedback = newFeedbackBuilder().extra(extraData).build();
        Assertions.assertTrue(evaluator.test(feedback));
    }

    @Test
    public void testTransitiveOverUndefined() {
        FeedbackDto feedback = newFeedbackBuilder().build();

        ExpressionEvaluatorOnJSEngine evaluator = wrap("f.extra.toto.tata == 'azerty'");
        Assertions.assertThrows(ExpressionException.class, () -> evaluator.test(feedback));

        ExpressionEvaluatorOnJSEngine evaluator2 = wrap("!f.extra.toto || f.extra.toto.tata == 'azerty'");
        Assertions.assertTrue(evaluator2.test(feedback));
    }

    @Test
    public void testDifferentTypes() {
        // Taille du tableau
        ExpressionEvaluatorOnJSEngine evaluator = wrap("f.extra.players.length == 4");

        {
            Map<String, Object> extraData = ImmutableMap.of(
                    "players", Arrays.asList("Elie", "Morgan", "Arthur", "Aloy")
            );
            FeedbackDto feedback = newFeedbackBuilder().extra(extraData).build();
            Assertions.assertTrue(evaluator.test(feedback));
        }

        {
            Map<String, Object> extraData = ImmutableMap.of(
                    "players", "Joël, Alyx, John, Rost"
            );
            FeedbackDto feedback = newFeedbackBuilder().extra(extraData).build();
            Assertions.assertFalse(evaluator.test(feedback));

            // Taille de le chaine de caractères
            evaluator = wrap("f.extra.players.length == 22");
            Assertions.assertTrue(evaluator.test(feedback));
        }
    }

}