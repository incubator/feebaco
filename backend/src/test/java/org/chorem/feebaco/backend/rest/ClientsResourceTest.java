package org.chorem.feebaco.backend.rest;

import io.quarkus.test.junit.QuarkusTest;
import org.chorem.feebaco.backend.rest.dtos.ImmutableClientInputDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.UUID;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static io.restassured.path.json.JsonPath.from;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;

@QuarkusTest
class ClientsResourceTest {

    @Test
    public void testCreateClient() {

        // On compte les clients avant
        int countBefore = countClients();

        // On créé l'objet en entrée
        String clientName = "taiste";
        ImmutableClientInputDto input = ImmutableClientInputDto.builder().name(clientName).build();
        // ... et on fait l'appel de création
        String clientId =
                given()
                    .body(input)
                    .contentType(MediaType.APPLICATION_JSON)
                    .post("/api/v1/clients")
                .then()
                    .statusCode(200)
                    // response looks like :
                    //     {"name":"taiste","clientId":"9c1f4ed2-cfc4-4947-8ed2-6112bccfea26","projects":{}}
                    .body("name", equalTo(clientName))
                    .body("projects", notNullValue())
                    .body("projects", is(new HashMap<>()))
                .extract()
                    .path("clientId");

        // On vérifie qu'un nouveau client a été créé
        int countAfter = countClients();
        Assertions.assertEquals(countBefore + 1, countAfter);

        // On refait un appel pour charger le client et on vérifie le nom
        given()
                .get("/api/v1/clients/{clientId}", clientId)
            .then()
                .statusCode(200)
                .body("name", equalTo(clientName));
    }

    @Test
    public void testCreateClientErrors() {

        // Pas d'objet en entrée
        given()
                .contentType(MediaType.APPLICATION_JSON)
                .post("/api/v1/clients")
            .then()
                .statusCode(400)
                .body("error", equalTo("Client name is mandatory"));


        // On créé l'objet en entrée
        String clientName = UUID.randomUUID().toString();
        ImmutableClientInputDto input = ImmutableClientInputDto.builder().name(clientName).build();
        // ... et on fait l'appel de création
        given()
                .body(input)
                .contentType(MediaType.APPLICATION_JSON)
                .post("/api/v1/clients")
            .then()
                .statusCode(200);

        // On fait l'appel de création une seconde fois
        given()
                .body(input)
                .contentType(MediaType.APPLICATION_JSON)
                .post("/api/v1/clients")
            .then()
                .statusCode(400)
                .body("error", equalTo("A client already exists with this name"));
    }

    /**
     * Méthode qui liste les clients et prend le "count" dans la réponse.
     */
    private int countClients() {
        String json = get("/api/v1/clients").asString();
        // response looks like :
        //     {"elements":[],"count":0,"currentPage":{"pageNumber":0,"pageSize":-1,"orderClauses":[]}}
        int countBefore = from(json).getInt("count");
        return countBefore;
    }

}
