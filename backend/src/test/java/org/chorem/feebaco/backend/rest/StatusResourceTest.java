package org.chorem.feebaco.backend.rest;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.startsWith;

@QuarkusTest
public class StatusResourceTest {

    @Test
    public void testStatusEndpoint() {
        given()
            .get("/api/v1/status")
        .then()
            .statusCode(200)
            .body("javaVersion", startsWith("16")) // Java 16, 16.*
            .body("databaseVersion", startsWith("0.")); // Ok tant qu'on est sur une version 0.x
    }

}
