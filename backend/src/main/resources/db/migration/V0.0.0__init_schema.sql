--
-- PostgreSQL database dump
--

-- Dumped from database version 13.3 (Debian 13.3-1.pgdg100+1)
-- Dumped by pg_dump version 13.3 (Debian 13.3-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: action_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.action_type AS ENUM (
    'STORE',
    'REDMINE_ISSUE',
    'GITLAB_ISSUE',
    'EMAIL'
);


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: action; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.action (
    actionid uuid NOT NULL,
    parameters jsonb,
    template character varying(255),
    type public.action_type,
    strategy_strategyid uuid NOT NULL
);


--
-- Name: client; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.client (
    clientid uuid NOT NULL,
    name character varying(255) NOT NULL
);


--
-- Name: feedback; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.feedback (
    feedbackid uuid NOT NULL,
    authoremail character varying(255),
    authorid character varying(255),
    backendversion character varying(255),
    browser character varying(255),
    category character varying(255) NOT NULL,
    createdon timestamp without time zone NOT NULL,
    description character varying(255),
    devicepixelratio character varying(255),
    displaysize character varying(255),
    frontendversion character varying(255),
    locale character varying(255),
    location character varying(255),
    locationtitle character varying(255),
    os character varying(255),
    platform character varying(255),
    screenresolution character varying(255),
    title character varying(255) NOT NULL,
    project_projectid uuid NOT NULL,
    extra jsonb
);


--
-- Name: project; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.project (
    projectid uuid NOT NULL,
    accesskey character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    client_clientid uuid NOT NULL,
    categories jsonb NOT NULL
);

--
-- Name: strategy; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.strategy (
    strategyid uuid NOT NULL,
    expression character varying(255) NOT NULL,
    project_projectid uuid NOT NULL
);


--
-- Name: action action_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.action
    ADD CONSTRAINT action_pkey PRIMARY KEY (actionid);


--
-- Name: client client_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT client_pkey PRIMARY KEY (clientid);


--
-- Name: feedback feedback_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.feedback
    ADD CONSTRAINT feedback_pkey PRIMARY KEY (feedbackid);


--
-- Name: project project_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.project
    ADD CONSTRAINT project_pkey PRIMARY KEY (projectid);


--
-- Name: strategy strategy_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.strategy
    ADD CONSTRAINT strategy_pkey PRIMARY KEY (strategyid);


--
-- Name: client uk_4d82t96jta8yi74jic0erkvjg; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT uk_4d82t96jta8yi74jic0erkvjg UNIQUE (name);


--
-- Name: project uklmfp8byqwanrtd39i66j3h73b; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.project
    ADD CONSTRAINT uklmfp8byqwanrtd39i66j3h73b UNIQUE (client_clientid, name);


--
-- Name: strategy fk4hm9w8b2axpyouqlfg3uiyhp7; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.strategy
    ADD CONSTRAINT fk4hm9w8b2axpyouqlfg3uiyhp7 FOREIGN KEY (project_projectid) REFERENCES public.project(projectid);


--
-- Name: action fk9cokfn869omytvlhdy4ynvtig; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.action
    ADD CONSTRAINT fk9cokfn869omytvlhdy4ynvtig FOREIGN KEY (strategy_strategyid) REFERENCES public.strategy(strategyid);


--
-- Name: feedback fkm5pwxs318k7gqa53oyocyyo5; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.feedback
    ADD CONSTRAINT fkm5pwxs318k7gqa53oyocyyo5 FOREIGN KEY (project_projectid) REFERENCES public.project(projectid);


--
-- Name: project fkr69borbhp7u55xl7wuckb9842; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.project
    ADD CONSTRAINT fkr69borbhp7u55xl7wuckb9842 FOREIGN KEY (client_clientid) REFERENCES public.client(clientid);


--
-- PostgreSQL database dump complete
--

