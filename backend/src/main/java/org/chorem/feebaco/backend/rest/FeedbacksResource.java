package org.chorem.feebaco.backend.rest;

import com.google.common.base.Preconditions;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import org.apache.commons.lang3.StringUtils;
import org.chorem.feebaco.backend.connectors.Connector;
import org.chorem.feebaco.backend.connectors.ConnectorFactory;
import org.chorem.feebaco.backend.database.FeedbackRepository;
import org.chorem.feebaco.backend.database.ProjectRepository;
import org.chorem.feebaco.backend.database.entities.Action;
import org.chorem.feebaco.backend.database.entities.ActionType;
import org.chorem.feebaco.backend.database.entities.Feedback;
import org.chorem.feebaco.backend.database.entities.Project;
import org.chorem.feebaco.backend.database.entities.Strategy;
import org.chorem.feebaco.backend.evaluator.ExpressionEvaluatorOnJSEngine;
import org.chorem.feebaco.backend.rest.dtos.FeedbackDto;
import org.chorem.feebaco.backend.rest.dtos.FeedbackInputDto;
import org.chorem.feebaco.backend.rest.dtos.ImmutableFeedbackDto;
import org.jboss.logging.Logger;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.time.ZonedDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Path("/api/v1")
@Produces(MediaType.APPLICATION_JSON)
@Transactional
public class FeedbacksResource extends AbstractFeebacoResource {

    @Inject
    protected FeedbackRepository feedbackRepository;

    @Inject
    protected ProjectRepository projectRepository;

    @Inject
    protected Logger log;

    @Inject
    protected ConnectorFactory connectors;

    @GET
    @Path("feedbacks")
    public PaginationResult<ImmutableFeedbackDto> getAllFeedbacks() {
        PanacheQuery<Feedback> query = feedbackRepository.findAll();
        List<Feedback> feedbacks = query.list();
        PaginationResult<ImmutableFeedbackDto> result = PaginationResult.fromFullList(feedbacks, PaginationParameter.ALL)
                .transform(FeedbackDto::of);
        return result;
    }

    @GET
    @Path("projects/{projectId}/feedbacks")
    public PaginationResult<ImmutableFeedbackDto> getProjectFeedbacks(@PathParam("projectId") UUID projectId) {
        Project project = projectRepository.findByProjectId(projectId);
        checkFound(project, "Project not found");
        List<Feedback> feedbacks = feedbackRepository.findAllByProject(project);
        PaginationResult<ImmutableFeedbackDto> result = PaginationResult.fromFullList(feedbacks, PaginationParameter.ALL)
                .transform(FeedbackDto::of);
        return result;
    }

    protected void checkAccessKey(Project project, String accessKey) throws NotAuthorizedException, ForbiddenException {
        if (StringUtils.isEmpty(accessKey)) {
            throw new NotAuthorizedException("Missing X-Feebaco-AccessKey header");
        }
        if (!project.getAccessKey().equals(accessKey)) {
            throw new ForbiddenException("Invalid access key");
        }
    }

    @POST
    @Path("projects/{projectId}/feedbacks")
    @Consumes(MediaType.APPLICATION_JSON)
    public ImmutableFeedbackDto createFeedback(@PathParam("projectId") UUID projectId,
                                               FeedbackInputDto input,
                                               @HeaderParam("X-Feebaco-AccessKey") String accessKey) {

        ImmutableFeedbackDto feedback = ImmutableFeedbackDto.builder()
                .from(input)
                .feedbackId(UUID.randomUUID())
                .createdOn(ZonedDateTime.now())
                .build();

        Project project = projectRepository.findByProjectId(projectId);
        checkFound(project, "Project not found");
        checkAccessKey(project, accessKey);
        Preconditions.checkArgument(project.getCategories().contains(input.getCategory()), "Invalid category, please refer to project categories");

        List<Action> actions = computeActions(project, feedback);

        if (actions.isEmpty()) {
            log.warn(String.format("Aucune action pour le feedback %s sur le projet %s", feedback, projectId));
        } else {
            List<ActionType> actionTypes = actions.stream()
                    .map(Action::getType)
                    .collect(Collectors.toList());
            log.warn(String.format("Applying actions %s for feedback %s", actionTypes, feedback));
            // FIXME AThimel 18/05/2021 L'application des actions devrait se faire de manière asynchrone pour rendre rapidement la main aux utilisateurs
            actions.forEach(a -> applyAction(a, feedback));
        }

        return feedback;
    }

    protected List<Action> computeActions(Project project, FeedbackDto feedback) {
        List<Action> result = new LinkedList<>();
        for (Strategy strategy : project.getStrategies()) {
            Predicate<FeedbackDto> predicate = new ExpressionEvaluatorOnJSEngine(strategy);
            boolean accepted = predicate.test(feedback);
            if (accepted) {
                List<Action> actions = strategy.getActions();
                if (actions.isEmpty()) {
                    log.warn("Aucune action associée à la stratégie " + strategy.getStrategyId());
                }
                result.addAll(actions);
            }
        }
        return result;
    }

    protected void applyAction(Action action, FeedbackDto feedback) {
        Connector connector = connectors.findConnector(action.getType());
        connector.sendFeedback(feedback, action);
    }

}
