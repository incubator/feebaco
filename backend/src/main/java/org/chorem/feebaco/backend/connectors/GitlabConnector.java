package org.chorem.feebaco.backend.connectors;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.StringUtils;
import org.chorem.feebaco.backend.FeebacoTechnicalException;
import org.chorem.feebaco.backend.database.entities.Action;
import org.chorem.feebaco.backend.rest.dtos.FeedbackDto;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class GitlabConnector implements Connector {

    protected TemplateHelper templates;

    public GitlabConnector(TemplateHelper templates) {
        this.templates = templates;
    }

    public void sendFeedback(FeedbackDto feedback, Action action) {
        Map<String, String> parameters = action.getParameters();
        Preconditions.checkState(parameters != null, "Paramètres manquants");
        Preconditions.checkState(parameters.containsKey("url"), "Paramètre manquant : url");
        Preconditions.checkState(parameters.containsKey("apiKey"), "Paramètre manquant : apiKey");

        try {
            Map<String, Object> issue = new HashMap<>();
            issue.put("title", String.format("Feedback : [%s] %s", feedback.getCategory(), feedback.getTitle()));

            Map<String, Object> templateParameters = ImmutableMap.of(
                    "f", feedback,
                    "extraEntries", feedback.getExtra().entrySet()
            );
            String issueDescription = Optional.ofNullable(action.getTemplate())
                    .map(template -> templates.render(template, templateParameters))
                    .orElseGet(() -> templates.renderResource("templates/gitlab.mustache", templateParameters));

            issue.put("description", issueDescription);

            HttpClient httpClient = HttpClient.newBuilder()
                    .version(HttpClient.Version.HTTP_1_1)
                    .build();

            String url = parameters.get("url");

            ObjectMapper objectMapper = new ObjectMapper();
            String issueJson = objectMapper.writeValueAsString(issue);

            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(url))
                    .setHeader("Content-Type", "application/json")
                    .setHeader("PRIVATE-TOKEN", parameters.get("apiKey"))
                    .POST(HttpRequest.BodyPublishers.ofString(issueJson))
                    .build();

            HttpResponse response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
//            System.out.println("Status  : " + response.statusCode());
//            System.out.println("Headers : " + response.headers());
//            System.out.println("Body    : " + response.body());
        } catch (IOException | InterruptedException e) {
            throw new FeebacoTechnicalException("Impossible de créer l'issue Gitlab", e);
        }
    }

    @Override
    public void validateAction(Action action) {
        Map<String, String> parameters = action.getParameters();
        Preconditions.checkArgument(parameters != null, "Missing parameters");
        Preconditions.checkArgument(StringUtils.isNotEmpty(parameters.get("url")), "Missing parameter: url");
        Preconditions.checkArgument(StringUtils.isNotEmpty(parameters.get("apiKey")), "Missing parameter: apiKey");
    }
}
