package org.chorem.feebaco.backend.database;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.chorem.feebaco.backend.database.entities.Project;
import org.chorem.feebaco.backend.database.entities.Strategy;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.UUID;

@ApplicationScoped
public class StrategyRepository implements PanacheRepository<Strategy> {

    public List<Strategy> findAllByProject(Project project) {
        List<Strategy> result = find("project", project).list();
        return result;
    }

    public Strategy findByStrategyId(UUID strategyId) {
        Strategy result = find("strategyId", strategyId).firstResult();
        return result;
    }
}
