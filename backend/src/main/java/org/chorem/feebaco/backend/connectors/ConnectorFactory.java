package org.chorem.feebaco.backend.connectors;

import org.chorem.feebaco.backend.FeebacoConfig;
import org.chorem.feebaco.backend.database.FeedbackRepository;
import org.chorem.feebaco.backend.database.entities.ActionType;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

@RequestScoped
public class ConnectorFactory {

    @Inject
    protected FeedbackRepository feedbackRepository;

    @Inject
    protected FeebacoConfig config;

    @Inject
    protected TemplateHelper templates;

    public Connector findConnector(ActionType type) {
        switch (type) {
            case STORE -> {
                return new StoreConnector(feedbackRepository);
            }
            case REDMINE_ISSUE -> {
                return new RedmineConnector(templates);
            }
            case GITLAB_ISSUE -> {
                return new GitlabConnector(templates);
            }
            case EMAIL -> {
                return new EmailConnector(config, templates);
            }
            default -> throw new UnsupportedOperationException("Action non implémentée : " + type);
        }
    }

}
