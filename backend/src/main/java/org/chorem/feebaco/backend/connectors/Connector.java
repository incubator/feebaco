package org.chorem.feebaco.backend.connectors;

import org.chorem.feebaco.backend.database.entities.Action;
import org.chorem.feebaco.backend.rest.dtos.FeedbackDto;

public interface Connector {

    void sendFeedback(FeedbackDto feedback, Action action);

    void validateAction(Action action);

}
