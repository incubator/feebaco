package org.chorem.feebaco.backend.rest.dtos;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import java.util.Map;
import java.util.UUID;

@Value.Immutable
@Value.Style(depluralize = true)
@JsonSerialize(as = ImmutableClientDto.class)
public interface ClientDto extends ClientInputDto {

    UUID getClientId();

    Map<UUID, String> getProjects();

}
