package org.chorem.feebaco.backend.database.entities;

public interface WithExpression {

    String getExpression();

}
