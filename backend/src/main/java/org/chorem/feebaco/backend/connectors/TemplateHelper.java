package org.chorem.feebaco.backend.connectors;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;

import javax.enterprise.context.ApplicationScoped;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Map;

@ApplicationScoped
public class TemplateHelper {

    protected String renderMustache(Mustache mustache, Map<String, Object> parameters) {
        StringWriter stringWriter = new StringWriter();
        mustache.execute(stringWriter, parameters);
        String result = stringWriter.toString();
        return result;
    }

    public String render(String template, Map<String, Object> templateParameters) {
        MustacheFactory mf = new DefaultMustacheFactory();
        StringReader reader = new StringReader(template);
        Mustache mustache = mf.compile(reader, "whatever");
        String result = renderMustache(mustache, templateParameters);
        return result;
    }

    public String renderResource(String templatePath, Map<String, Object> templateParameters) {
        MustacheFactory mf = new DefaultMustacheFactory();
        Mustache mustache = mf.compile(templatePath);
        String result = renderMustache(mustache, templateParameters);
        return result;
    }

}
