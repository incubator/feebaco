package org.chorem.feebaco.backend.database.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;
import java.util.UUID;

@Entity
public class Strategy implements WithExpression {

    @Id
    @GeneratedValue
    protected UUID strategyId;

    @Column(nullable = false)
    protected String expression;

    @OneToMany(mappedBy = "strategy", cascade = CascadeType.ALL)
    protected List<Action> actions;

    @ManyToOne(optional = false)
    protected Project project;

    public UUID getStrategyId() {
        return strategyId;
    }

    public void setStrategyId(UUID strategyId) {
        this.strategyId = strategyId;
    }

    @Override
    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public List<Action> getActions() {
        return actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    @Override
    public String toString() {
        return "Strategy{" +
                "strategyId=" + strategyId +
                ", expression='" + expression + '\'' +
                ", actions=" + actions +
                '}';
    }
}
