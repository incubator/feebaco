package org.chorem.feebaco.backend.rest.dtos;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.apache.commons.collections4.MapUtils;
import org.chorem.feebaco.backend.database.entities.Feedback;
import org.immutables.value.Value;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.UUID;

@Value.Immutable
@Value.Style(depluralize = true)
@JsonSerialize(as = ImmutableFeedbackDto.class)
public interface FeedbackDto extends FeedbackInputDto {

    UUID getFeedbackId();

    ZonedDateTime getCreatedOn();

    static ImmutableFeedbackDto of(Feedback f) {
        ImmutableFeedbackDto result = ImmutableFeedbackDto.builder()
                .feedbackId(f.getFeedbackId())
                .createdOn(f.getCreatedOn())

                .category(f.getCategory())
                .title(f.getTitle())
                .description(Optional.ofNullable(f.getDescription()))

                .authorId(Optional.ofNullable(f.getAuthorId()))
                .authorEmail(Optional.ofNullable(f.getAuthorEmail()))

                .location(Optional.ofNullable(f.getLocation()))
                .locationTitle(Optional.ofNullable(f.getLocationTitle()))
                .locale(Optional.ofNullable(f.getLocale()))
                .frontendVersion(Optional.ofNullable(f.getFrontendVersion()))
                .backendVersion(Optional.ofNullable(f.getBackendVersion()))

                .browser(Optional.ofNullable(f.getBrowser()))
                .os(Optional.ofNullable(f.getOs()))
                .platform(Optional.ofNullable(f.getPlatform()))
                .screenResolution(Optional.ofNullable(f.getScreenResolution()))
                .devicePixelRatio(Optional.ofNullable(f.getDevicePixelRatio()))
                .displaySize(Optional.ofNullable(f.getDisplaySize()))

                .extra(MapUtils.emptyIfNull(f.getExtra()))

                .build();
        return result;
    }

}
