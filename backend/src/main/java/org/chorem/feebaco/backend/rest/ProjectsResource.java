package org.chorem.feebaco.backend.rest;

import com.google.common.base.Preconditions;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.chorem.feebaco.backend.connectors.Connector;
import org.chorem.feebaco.backend.connectors.ConnectorFactory;
import org.chorem.feebaco.backend.database.ActionRepository;
import org.chorem.feebaco.backend.database.ClientRepository;
import org.chorem.feebaco.backend.database.ProjectRepository;
import org.chorem.feebaco.backend.database.StrategyRepository;
import org.chorem.feebaco.backend.database.entities.Action;
import org.chorem.feebaco.backend.database.entities.ActionType;
import org.chorem.feebaco.backend.database.entities.Client;
import org.chorem.feebaco.backend.database.entities.Project;
import org.chorem.feebaco.backend.database.entities.Strategy;
import org.chorem.feebaco.backend.evaluator.Constants;
import org.chorem.feebaco.backend.rest.dtos.ActionDto;
import org.chorem.feebaco.backend.rest.dtos.ImmutableProjectDto;
import org.chorem.feebaco.backend.rest.dtos.ImmutableStrategyDto;
import org.chorem.feebaco.backend.rest.dtos.ProjectDto;
import org.chorem.feebaco.backend.rest.dtos.ProjectInputDto;
import org.chorem.feebaco.backend.rest.dtos.StrategyDto;
import org.chorem.feebaco.backend.rest.dtos.StrategyInputDto;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

@Path("/api/v1")
@Produces(MediaType.APPLICATION_JSON)
@Transactional
public class ProjectsResource extends AbstractFeebacoResource {

    @Inject
    protected ClientRepository clientRepository;

    @Inject
    protected ProjectRepository projectRepository;

    @Inject
    protected StrategyRepository strategyRepository;

    @Inject
    protected ActionRepository actionRepository;

    @Inject
    protected ConnectorFactory connectors;

    @GET
    @Path("projects")
    public PaginationResult<ImmutableProjectDto> getAllProject() {
        PanacheQuery<Project> query = projectRepository.findAll();
        List<Project> projects = query.list();
        PaginationResult<ImmutableProjectDto> result = PaginationResult.fromFullList(projects, PaginationParameter.ALL)
                .transform(ProjectDto::of);
        return result;
    }

    @GET
    @Path("clients/{clientId}/projects")
    public PaginationResult<ImmutableProjectDto> getClientProjects(@PathParam("clientId") UUID clientId) {
        Client client = clientRepository.findByClientId(clientId);
        checkFound(client, "Client not found");
        List<Project> projects = projectRepository.findAllByClient(client);
        PaginationResult<ImmutableProjectDto> result = PaginationResult.fromFullList(projects, PaginationParameter.ALL)
                .transform(ProjectDto::of);
        return result;
    }

    @POST
    @Path("clients/{clientId}/projects")
    public ImmutableProjectDto createProject(@PathParam("clientId") UUID clientId,
                                             ProjectInputDto input) {
        Client client = clientRepository.findByClientId(clientId);
        checkFound(client, "Client not found");
        Preconditions.checkArgument(StringUtils.isNotEmpty(input.getName()), "Project's name is mandatory");
        Preconditions.checkArgument(CollectionUtils.isNotEmpty(input.getCategories()), "Project's feedback's categories are mandatory");
        Preconditions.checkArgument(input.getCategories().stream().noneMatch(StringUtils::isEmpty), "Empty category is not allowed");
        List<Project> clientProjects = projectRepository.findAllByClient(client);
        boolean nameConflict = clientProjects.stream()
                .map(Project::getName)
                .anyMatch(n -> n.equals(input.getName()));
        Preconditions.checkArgument(!nameConflict, "A project already exists with this name");

        Project project = new Project();
        project.setClient(client);
        project.setName(input.getName());
        project.setAccessKey(RandomStringUtils.randomAlphanumeric(32));
        project.setCategories(input.getCategories());
        projectRepository.persist(project);

        Strategy defaultStrategy = new Strategy();
        ArrayList<Action> actions = new ArrayList<>();
        defaultStrategy.setActions(actions);
        defaultStrategy.setExpression(Constants.DEFAULT_STRATEGY_EXPRESSION);
        defaultStrategy.setProject(project);
        strategyRepository.persist(defaultStrategy);

        Action defaultAction = new Action();
        defaultAction.setType(ActionType.STORE);
        defaultAction.setStrategy(defaultStrategy);
        actionRepository.persist(defaultAction);

        ImmutableProjectDto result = ProjectDto.of(project);
        return result;
    }

    @GET
    @Path("projects/{projectId}/strategies")
    public List<ImmutableStrategyDto> getProjectStrategies(@PathParam("projectId") UUID projectId) {
        Project project = projectRepository.findByProjectId(projectId);
        checkFound(project, "Project not found");

        List<Strategy> strategies = strategyRepository.findAllByProject(project);
        List<ImmutableStrategyDto> result = strategies.stream()
                .map(StrategyDto::of)
                .collect(Collectors.toList());
        return result;
    }

    @POST
    @Path("projects/{projectId}/strategies")
    public ImmutableStrategyDto createProjectStrategy(@PathParam("projectId") UUID projectId, StrategyInputDto input) {
        Project project = projectRepository.findByProjectId(projectId);
        checkFound(project, "Project not found");

        Strategy newStrategy = new Strategy();
        ArrayList<Action> actions = new ArrayList<>();
        newStrategy.setActions(actions);
        newStrategy.setExpression(input.getExpression());
        newStrategy.setProject(project);

        Function<ActionDto, Action> dtoToActionFunction = a -> {
            Action action = new Action();
            action.setType(a.type());
            action.setStrategy(newStrategy);
            action.setTemplate(a.template());
            action.setParameters(a.parameters());
            return action;
        };
        input.getActions()
                .stream()
                .map(dtoToActionFunction)
                .forEach(a -> {
                    Connector connector = connectors.findConnector(a.getType());
                    connector.validateAction(a);
                    newStrategy.getActions().add(a);
                });
        strategyRepository.persist(newStrategy);

        ImmutableStrategyDto result = StrategyDto.of(newStrategy);
        return result;
    }

    @DELETE
    @Path("projects/{projectId}/strategies/{strategyId}")
    public void createProjectStrategy(@PathParam("projectId") UUID projectId, @PathParam("strategyId") UUID strategyId) {
        Strategy strategy = strategyRepository.findByStrategyId(strategyId);
        checkFound(strategy, "Strategy not found");

        Preconditions.checkArgument(strategy.getProject().getProjectId().equals(projectId), "Project mismatch");

        strategyRepository.delete(strategy);
    }

}
