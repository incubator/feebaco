package org.chorem.feebaco.backend.rest;

import javax.ws.rs.NotFoundException;

public abstract class AbstractFeebacoResource {

    protected void checkFound(Object anyObject, String message) throws NotFoundException {
        if (anyObject == null) {
            throw new NotFoundException(message);
        }
    }

}
