package org.chorem.feebaco.backend;

import io.quarkus.arc.config.ConfigProperties;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import java.util.Properties;

@ConfigProperties(prefix = "feebaco")
public interface FeebacoConfig {

    String getVersion();

    String getGitRevision();

    default String getFullVersion() {
        String result = String.format("%s (%s)", getVersion(), getGitRevision());
        return result;
    }

    String getBuildDate();

    String getSiteUrlRoot();

    String getSmtpHost();

    String getSmtpPort();

    @ConfigProperty(defaultValue = "feebaco@chorem.org")
    String getMailFrom();

    default Properties getMailProperties() {
//        boolean auth = getSmtpUsername().isPresent() && getSmtpPassword().isPresent();
        boolean auth = false;
        Properties prop = new Properties();
        prop.put("mail.smtp.auth", auth);
        prop.put("mail.smtp.starttls.enable", String.valueOf(false));
        prop.put("mail.smtp.host", getSmtpHost());
        prop.put("mail.smtp.port", String.valueOf(getSmtpPort()));
        return prop;
    }

}
