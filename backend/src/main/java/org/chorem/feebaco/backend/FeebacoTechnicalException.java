package org.chorem.feebaco.backend;


public class FeebacoTechnicalException extends RuntimeException {

    public FeebacoTechnicalException(String message, Throwable cause) {
        super(message, cause);
    }

}