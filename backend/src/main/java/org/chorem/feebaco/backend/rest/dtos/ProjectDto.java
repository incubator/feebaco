package org.chorem.feebaco.backend.rest.dtos;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.chorem.feebaco.backend.database.entities.Project;
import org.immutables.value.Value;

import java.util.UUID;

@Value.Immutable
@Value.Style(depluralize = true)
@JsonSerialize(as = ImmutableProjectDto.class)
public interface ProjectDto extends ProjectInputDto {

    UUID getProjectId();

    String getAccessKey();

    static ImmutableProjectDto of(Project p) {
        ImmutableProjectDto result = ImmutableProjectDto.builder()
                .projectId(p.getProjectId())
                .name(p.getName())
                .accessKey(p.getAccessKey())
                .categories(p.getCategories())
                .build();
        return result;
    }

}
