package org.chorem.feebaco.backend.rest.mappers;

import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.BooleanNode;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.TextNode;
import org.apache.commons.lang3.StringUtils;

import java.util.Optional;
import java.util.UUID;

public abstract class AbstractSimpleModule extends SimpleModule {

    static boolean readBoolean(TreeNode node, String name) {
        TreeNode subNode = node.get(name);
        boolean result;
        if (subNode instanceof BooleanNode) {
            result = ((BooleanNode) subNode).booleanValue();
        } else if (subNode instanceof TextNode) {
            String resultString = ((TextNode) subNode).textValue();
            result = Boolean.parseBoolean(resultString);
        } else {
            throw new IllegalArgumentException("Unexpected type:" + subNode.getClass().getName());
        }
        return result;
    }

    static int readInt(TreeNode node, String name) {
        TreeNode subNode = node.get(name);
        int result;
        if (subNode instanceof IntNode) {
            result = ((IntNode) subNode).intValue();
        } else if (subNode instanceof TextNode) {
            String resultString = ((TextNode) subNode).textValue();
            result = Integer.parseInt(resultString);
        } else {
            throw new IllegalArgumentException("Unexpected type:" + subNode.getClass().getName());
        }
        return result;
    }

    static Integer readIntegerOrNull(TreeNode node, String name) {
        TreeNode subNode = node.get(name);
        if (subNode == null || (subNode instanceof NullNode) || subNode.isMissingNode()) {
            return null;
        }
        int result;
        if (subNode instanceof IntNode) {
            result = ((IntNode) subNode).intValue();
        } else if (subNode instanceof TextNode) {
            String resultString = ((TextNode) subNode).textValue();
            if (StringUtils.isEmpty(resultString)) {
                return null;
            }
            result = Integer.parseInt(resultString);
        } else {
            throw new IllegalArgumentException("Unexpected type:" + subNode.getClass().getName());
        }
        return result;
    }

    static Optional<Integer> readInteger(TreeNode node, String name) {
        Integer value = readIntegerOrNull(node, name);
        Optional<Integer> result = Optional.ofNullable(value);
        return result;
    }

    static String readTextOrNull(TreeNode node, String name) {
        TreeNode subNode = node.get(name);
        if (subNode == null || (subNode instanceof NullNode) || subNode.isMissingNode()) {
            return null;
        }
        String value = ((TextNode) subNode).textValue();
        String result = StringUtils.trimToNull(value);
        return result;
    }

    static Optional<String> readText(TreeNode node, String name) {
        String value = readTextOrNull(node, name);
        Optional<String> result = Optional.ofNullable(value);
        return result;
    }

    static Optional<UUID> readUuid(TreeNode node, String name) {
        Optional<UUID> result = readText(node, name).map(UUID::fromString);
        return result;
    }

    static UUID readUuidOrNull(TreeNode node, String name) {
        UUID result = readUuid(node, name).orElse(null);
        return result;
    }

}
