package org.chorem.feebaco.backend.rest;

import com.google.common.base.Preconditions;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import org.apache.commons.lang3.StringUtils;
import org.chorem.feebaco.backend.database.ClientRepository;
import org.chorem.feebaco.backend.database.ProjectRepository;
import org.chorem.feebaco.backend.database.entities.Client;
import org.chorem.feebaco.backend.database.entities.Project;
import org.chorem.feebaco.backend.rest.dtos.ClientDto;
import org.chorem.feebaco.backend.rest.dtos.ClientInputDto;
import org.chorem.feebaco.backend.rest.dtos.ImmutableClientDto;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.UUID;

@Path("/api/v1")
@Produces(MediaType.APPLICATION_JSON)
@Transactional
public class ClientsResource extends AbstractFeebacoResource {

    @Inject
    protected ClientRepository clientRepository;

    @Inject
    protected ProjectRepository projectRepository;

    @GET
    @Path("clients")
    public PaginationResult<ImmutableClientDto> getAllClients() {
        PanacheQuery<Client> query = clientRepository.findAll();
        List<Client> clients = query.list();
        PaginationResult<ImmutableClientDto> result = PaginationResult.fromFullList(clients, PaginationParameter.ALL)
                .transform(this::toDto);
        return result;
    }

    @POST
    @Path("clients")
    @Consumes(MediaType.APPLICATION_JSON)
    public ClientDto createClient(ClientInputDto input) {
        Client client = new Client();
        Preconditions.checkArgument(input != null && StringUtils.isNotEmpty(input.getName()), "Client name is mandatory");

        boolean clientExists = clientRepository.clientExists(input.getName());
        Preconditions.checkArgument(!clientExists, "A client already exists with this name");

        client.setName(input.getName());
        clientRepository.persist(client);

        ImmutableClientDto result = toDto(client);
        return result;
    }

    @GET
    @Path("clients/{clientId}")
    public ImmutableClientDto getClient(@PathParam("clientId") UUID clientId) {
        Client client = clientRepository.findByClientId(clientId);
        checkFound(client, "Client not found");
        ImmutableClientDto result = toDto(client);
        return result;
    }

    protected ImmutableClientDto toDto(Client client) {
        List<Project> projects = projectRepository.findAllByClient(client);

        ImmutableClientDto.Builder builder = ImmutableClientDto.builder()
                .clientId(client.getClientId())
                .name(client.getName());
        projects.forEach(p -> builder.putProject(p.getProjectId(), p.getName()));
        ImmutableClientDto result = builder.build();
        return result;
    }

}
