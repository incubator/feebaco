package org.chorem.feebaco.backend.database;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.chorem.feebaco.backend.database.entities.Client;
import org.chorem.feebaco.backend.database.entities.Project;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.UUID;

@ApplicationScoped
public class ProjectRepository implements PanacheRepository<Project> {

    public List<Project> findAllByClient(Client client) {
        List<Project> result = find("client", client).list();
        return result;
    }

    public List<Project> findAllByClientId(UUID clientId) {
        List<Project> result = find("client.clientId", clientId).list();
        return result;
    }

    public Project findByProjectId(UUID projectId) {
        Project result = find("projectId", projectId).firstResult();
        return result;
    }
}
