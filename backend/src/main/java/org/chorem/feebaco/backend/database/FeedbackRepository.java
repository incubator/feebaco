package org.chorem.feebaco.backend.database;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.chorem.feebaco.backend.database.entities.Feedback;
import org.chorem.feebaco.backend.database.entities.Project;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.UUID;

@ApplicationScoped
public class FeedbackRepository implements PanacheRepository<Feedback> {

    public List<Feedback> findAllByProjectId(UUID projectId) {
        List<Feedback> result = find("project.projectId", projectId).list();
        return result;
    }

    public List<Feedback> findAllByProject(Project project) {
        List<Feedback> result = find("project", project).list();
        return result;
    }

}
