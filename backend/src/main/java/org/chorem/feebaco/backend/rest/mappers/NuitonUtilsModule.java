package org.chorem.feebaco.backend.rest.mappers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.common.base.Preconditions;
import org.nuiton.util.pagination.PaginationOrder;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class NuitonUtilsModule extends AbstractSimpleModule {

    public static class PaginationParameterDeserializer extends StdDeserializer<PaginationParameter> {

        protected PaginationParameterDeserializer() {
            super(PaginationParameter.class);
        }

        @Override
        public PaginationParameter deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
            TreeNode node = jp.readValueAsTree();

            TreeNode orderClausesNode = node.get("orderClauses");
            Preconditions.checkState(orderClausesNode.isArray());
            List<PaginationOrder> orders = new LinkedList<>();
            for (JsonNode n : (ArrayNode) orderClausesNode) {
                // TODO AThimel 13/01/2020 : Ça aurait été mieux de réutiliser le DeserializationContext
                String clause = readTextOrNull(n, "clause");
                boolean desc = readBoolean(n, "desc");
                PaginationOrder order = new PaginationOrder(clause, desc);
                orders.add(order);
            }

            int pageNumberValue = readInt(node, "pageNumber");
            int pageSizeValue = readInt(node, "pageSize");
            PaginationParameter.PaginationParameterBuilder builder = PaginationParameter.builder(pageNumberValue, pageSizeValue);
            builder.addOrderClauses(orders);
            PaginationParameter result = builder.build();
            return result;
        }
    }

    public NuitonUtilsModule() {
        addSerializer(PaginationResult.class, new JsonSerializer<>() {
            @Override
            public void serialize(PaginationResult value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
                gen.writeStartObject();
                gen.writeObjectField("elements", value.getElements());
                gen.writeObjectField("count", value.getCount());
                gen.writeObjectField("currentPage", value.getCurrentPage());
                gen.writeEndObject();
            }
        });
        addSerializer(PaginationParameter.class, new JsonSerializer<>() {
            @Override
            public void serialize(PaginationParameter value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
                gen.writeStartObject();
                gen.writeObjectField("pageNumber", value.getPageNumber());
                gen.writeObjectField("pageSize", value.getPageSize());
                gen.writeObjectField("orderClauses", value.getOrderClauses());
                gen.writeEndObject();
            }
        });

        addDeserializer(PaginationParameter.class, new PaginationParameterDeserializer());
    }

}
