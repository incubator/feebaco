package org.chorem.feebaco.backend.rest.dtos;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import java.util.Set;

@Value.Immutable
@Value.Style(depluralize = true)
@JsonDeserialize(as = ImmutableProjectInputDto.class)
public interface ProjectInputDto {

    String getName();

    Set<String> getCategories();

}
