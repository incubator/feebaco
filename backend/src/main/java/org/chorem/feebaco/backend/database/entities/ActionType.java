package org.chorem.feebaco.backend.database.entities;

public enum ActionType {
    STORE,
    REDMINE_ISSUE,
    GITLAB_ISSUE,
    EMAIL
}
