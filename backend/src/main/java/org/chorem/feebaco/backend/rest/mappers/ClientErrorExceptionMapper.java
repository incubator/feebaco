package org.chorem.feebaco.backend.rest.mappers;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.ext.Provider;

@Provider
public class ClientErrorExceptionMapper extends AbstractFeebacoExceptionMapper<ClientErrorException> {

    @Override
    protected int getStatusCode(ClientErrorException exception) {
        return exception.getResponse().getStatus();
    }

}
