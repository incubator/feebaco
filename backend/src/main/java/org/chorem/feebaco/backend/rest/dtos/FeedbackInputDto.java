package org.chorem.feebaco.backend.rest.dtos;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import java.util.Map;
import java.util.Optional;

@Value.Immutable
@Value.Style(depluralize = true)
@JsonDeserialize(as = ImmutableFeedbackInputDto.class)
public interface FeedbackInputDto {

    String getCategory();

    String getTitle();

    Optional<String> getDescription();

    // *** Author ***

    Optional<String> getAuthorId();

    Optional<String> getAuthorEmail();

    // *** Application data ***

    Optional<String> getLocation();

    Optional<String> getLocationTitle();

    Optional<String> getLocale();

    Optional<String> getFrontendVersion();

    Optional<String> getBackendVersion();

    // *** Technical data ***

    Optional<String> getBrowser();

    Optional<String> getOs();

    Optional<String> getPlatform();

    Optional<String> getScreenResolution();

    Optional<String> getDevicePixelRatio();

    Optional<String> getDisplaySize();

    // *** Extra data ***

    Map<String, Object> getExtra();

}
