package org.chorem.feebaco.backend.rest.dtos;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import java.util.List;

@Value.Immutable
@Value.Style(depluralize = true)
@JsonDeserialize(as = ImmutableStrategyInputDto.class)
public interface StrategyInputDto {

    String getExpression();

    List<ActionDto> getActions();

}
