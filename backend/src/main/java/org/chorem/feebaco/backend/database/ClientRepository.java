package org.chorem.feebaco.backend.database;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.chorem.feebaco.backend.database.entities.Client;

import javax.enterprise.context.ApplicationScoped;
import java.util.UUID;

@ApplicationScoped
public class ClientRepository implements PanacheRepository<Client> {

    public Client findByClientId(UUID clientId) {
        Client result = find("clientId", clientId).firstResult();
        return result;
    }

    public boolean clientExists(String name) {
        boolean result = find("name", name).count() > 0;
        return result;
    }
}
