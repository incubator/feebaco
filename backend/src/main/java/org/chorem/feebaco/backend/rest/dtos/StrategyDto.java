package org.chorem.feebaco.backend.rest.dtos;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.chorem.feebaco.backend.database.entities.Strategy;
import org.immutables.value.Value;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Value.Immutable
@Value.Style(depluralize = true)
@JsonSerialize(as = ImmutableStrategyDto.class)
public interface StrategyDto extends StrategyInputDto {

    UUID getStrategyId();

    static ImmutableStrategyDto of(Strategy s) {
        List<ActionDto> actionDtos = s.getActions()
                .stream()
                .map(ActionDto::new)
                .collect(Collectors.toList());
        ImmutableStrategyDto result = ImmutableStrategyDto.builder()
                .strategyId(s.getStrategyId())
                .expression(s.getExpression())
                .actions(actionDtos)
                .build();
        return result;
    }

}
