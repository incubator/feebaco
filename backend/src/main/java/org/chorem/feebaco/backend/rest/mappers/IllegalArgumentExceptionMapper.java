package org.chorem.feebaco.backend.rest.mappers;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

@Provider
public class IllegalArgumentExceptionMapper extends AbstractFeebacoExceptionMapper<IllegalArgumentException> {

    @Override
    protected int getStatusCode(IllegalArgumentException exception) {
        return Response.Status.BAD_REQUEST.getStatusCode();
    }

}
