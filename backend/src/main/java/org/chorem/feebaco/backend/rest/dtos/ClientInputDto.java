package org.chorem.feebaco.backend.rest.dtos;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

@Value.Immutable
@Value.Style(depluralize = true)
@JsonDeserialize(as = ImmutableClientInputDto.class)
public interface ClientInputDto {

    String getName();

}
