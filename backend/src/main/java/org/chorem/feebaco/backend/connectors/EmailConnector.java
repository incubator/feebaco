package org.chorem.feebaco.backend.connectors;

import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.validator.routines.EmailValidator;
import org.chorem.feebaco.backend.FeebacoConfig;
import org.chorem.feebaco.backend.FeebacoTechnicalException;
import org.chorem.feebaco.backend.database.entities.Action;
import org.chorem.feebaco.backend.rest.dtos.FeedbackDto;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

public class EmailConnector implements Connector {

    protected FeebacoConfig config;
    protected TemplateHelper templates;

    public EmailConnector(FeebacoConfig config, TemplateHelper templates) {
        this.config = config;
        this.templates = templates;
    }

    private MimeMessage buildMimeMessage(Iterable<String> tos, String subject, String body) {

        Properties prop = config.getMailProperties();

        Session session = Session.getInstance(prop);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(config.getMailFrom());
            for (String to : tos) {
                message.addRecipients(Message.RecipientType.TO, to);
            }
            message.setSubject(subject);

            MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setContent(body, "text/html;charset=utf-8");

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(mimeBodyPart);

            message.setContent(multipart);

        } catch (MessagingException me) {
            throw new FeebacoTechnicalException("Unable to build MimeMessage", me);
        }
        return message;
    }

    public void sendFeedback(FeedbackDto feedback, Action action) {
        Map<String, String> parameters = action.getParameters();
        Preconditions.checkState(parameters != null, "Paramètres manquants");
        Preconditions.checkState(parameters.containsKey("to"), "Paramètre manquant : to");

        try {

            String subject = String.format("Feedback : [%s] %s", feedback.getCategory(), feedback.getTitle());

            Map<String, Object> templateParameters = ImmutableMap.of(
                    "f", feedback,
                    "extraEntries", feedback.getExtra().entrySet()
            );
            String body = Optional.ofNullable(action.getTemplate())
                    .map(template -> templates.render(template, templateParameters))
                    .orElseGet(() -> templates.renderResource("templates/email.mustache", templateParameters));

            List<String> tos = Splitter.on(",")
                    .omitEmptyStrings()
                    .splitToList(parameters.get("to"));
            MimeMessage mimeMessage = buildMimeMessage(tos, subject, body);

            Transport.send(mimeMessage);
//            System.out.println("Status  : " + response.statusCode());
//            System.out.println("Headers : " + response.headers());
//            System.out.println("Body    : " + response.body());
        } catch (MessagingException me) {
            throw new FeebacoTechnicalException("Impossible d'envoyer le mail", me);
        }
    }

    @Override
    public void validateAction(Action action) {
        Map<String, String> parameters = action.getParameters();
        Preconditions.checkArgument(parameters != null, "Missing parameters");
        String tosString = parameters.get("to");
        Preconditions.checkArgument(tosString != null, "Missing parameter: to");
        List<String> tos = Splitter.on(",")
                .omitEmptyStrings()
                .splitToList(tosString);
        tos.forEach(to -> Preconditions.checkArgument(EmailValidator.getInstance().isValid(to), "Invalid email address: " + to));
        Preconditions.checkArgument(tos.size() >= 1, "At least one valid 'to' address is necessary");
    }

}
