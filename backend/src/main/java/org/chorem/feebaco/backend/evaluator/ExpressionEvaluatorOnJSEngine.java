package org.chorem.feebaco.backend.evaluator;

import com.google.common.base.Preconditions;
import org.chorem.feebaco.backend.database.entities.WithExpression;
import org.chorem.feebaco.backend.rest.dtos.FeedbackDto;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.function.Predicate;

public class ExpressionEvaluatorOnJSEngine implements Predicate<FeedbackDto> {

    protected final String expression;

    public ExpressionEvaluatorOnJSEngine(WithExpression strategy) {
        this.expression = strategy.getExpression();
    }

    protected boolean testExpression(String expression, FeedbackDto feedback) {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("nashorn");

        Preconditions.checkState(engine != null, "Nashorn engine is missing, check dependencies");

        Bindings bindings = engine.getBindings(ScriptContext.ENGINE_SCOPE);
        bindings.clear();
        bindings.put("f", feedback);
        Object evalResult;
        try {
            evalResult = engine.eval(expression, bindings);
        } catch (ScriptException e) {
            String message = String.format("L'expression %s a provoqué une erreur", expression);
            throw new ExpressionException(message, e);
        }
        boolean result = Boolean.TRUE.equals(evalResult);
        return result;
    }

    public boolean test(FeedbackDto feedback) {
        boolean result = testExpression(expression, feedback);
        return result;
    }

}
