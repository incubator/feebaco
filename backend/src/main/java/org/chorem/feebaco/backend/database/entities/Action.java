package org.chorem.feebaco.backend.database.entities;

import com.vladmihalcea.hibernate.type.basic.PostgreSQLEnumType;
import com.vladmihalcea.hibernate.type.json.JsonType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Map;
import java.util.UUID;

@Entity
@TypeDef(name = "json", typeClass = JsonType.class)
@TypeDef(name = "pgsql_enum", typeClass = PostgreSQLEnumType.class)
public class Action {

    @Id
    @GeneratedValue
    protected UUID actionId;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "action_type")
    @Type( type = "pgsql_enum" )
    protected ActionType type;

    protected String template;

    @Type(type = "json")
    @Column(columnDefinition = "jsonb")
    protected Map<String, String> parameters;

    @ManyToOne(optional = false)
    protected Strategy strategy;

    public UUID getActionId() {
        return actionId;
    }

    public void setActionId(UUID actionId) {
        this.actionId = actionId;
    }

    public ActionType getType() {
        return type;
    }

    public void setType(ActionType type) {
        this.type = type;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }

    public Strategy getStrategy() {
        return strategy;
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

    @Override
    public String toString() {
        return "Action{" +
                "actionId=" + actionId +
                ", type='" + type + '\'' +
                '}';
    }
}
