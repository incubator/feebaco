package org.chorem.feebaco.backend.rest.mappers;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import java.util.LinkedHashMap;
import java.util.Map;

public abstract class AbstractFeebacoExceptionMapper<T extends Throwable> implements ExceptionMapper<T> {

    protected abstract int getStatusCode(T exception);

    @Override
    public Response toResponse(T exception) {
        Response.ResponseBuilder responseBuilder = Response.status(getStatusCode(exception));
        Map<String, String> entity = new LinkedHashMap<>();
        if (StringUtils.isNotEmpty(exception.getMessage())) {
            entity.put("error", exception.getMessage());
        }
        Throwable cause = exception.getCause();
        if (cause != null) {
            entity.put("cause", cause.getClass().getName() + ": " + cause.getMessage());
        }
        if (!entity.isEmpty()) {
            responseBuilder.entity(entity);
        }

        Log log = LogFactory.getLog(this.getClass());
        if (log.isWarnEnabled()) {
            log.warn(String.format("%s thrown: %s", exception.getClass().getName(), entity));
        }

        Response result = responseBuilder.build();
        return result;
    }

}
