package org.chorem.feebaco.backend.evaluator;

public class ExpressionException extends RuntimeException {

    public ExpressionException(String message, Throwable cause) {
        super(message, cause);
    }

}
