package org.chorem.feebaco.backend;

import io.agroal.api.AgroalDataSource;
import io.quarkus.runtime.StartupEvent;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

@ApplicationScoped
public class FeebacoApplication {

    @Inject
    protected AgroalDataSource ds;

    void onStart(@Observes StartupEvent ev) {
        // Nothing to do on startup ?
    }

}
