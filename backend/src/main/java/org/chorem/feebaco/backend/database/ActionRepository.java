package org.chorem.feebaco.backend.database;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.chorem.feebaco.backend.database.entities.Action;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ActionRepository implements PanacheRepository<Action> {

}
