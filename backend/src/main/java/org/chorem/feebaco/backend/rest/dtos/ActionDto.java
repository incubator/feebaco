package org.chorem.feebaco.backend.rest.dtos;

import org.chorem.feebaco.backend.database.entities.Action;
import org.chorem.feebaco.backend.database.entities.ActionType;

import java.util.Map;

public record ActionDto(ActionType type, String template, Map<String, String> parameters) {

    ActionDto(Action source) {
        this(source.getType(), source.getTemplate(), source.getParameters());
    }

}
