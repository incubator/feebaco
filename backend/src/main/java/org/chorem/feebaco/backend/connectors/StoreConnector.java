package org.chorem.feebaco.backend.connectors;

import org.chorem.feebaco.backend.database.FeedbackRepository;
import org.chorem.feebaco.backend.database.entities.Action;
import org.chorem.feebaco.backend.database.entities.Feedback;
import org.chorem.feebaco.backend.database.entities.Project;
import org.chorem.feebaco.backend.rest.dtos.FeedbackDto;

import java.time.ZonedDateTime;

public class StoreConnector implements Connector {

    protected FeedbackRepository feedbackRepository;

    public StoreConnector(FeedbackRepository feedbackRepository) {
        this.feedbackRepository = feedbackRepository;
    }

    @Override
    public void sendFeedback(FeedbackDto input, Action action) {

        Project project = action.getStrategy().getProject();

        Feedback feedback = new Feedback();
        feedback.setProject(project);
        feedback.setFeedbackId(input.getFeedbackId());
        feedback.setCreatedOn(ZonedDateTime.now());

        feedback.setCategory(input.getCategory());
        feedback.setTitle(input.getTitle());
        input.getDescription().ifPresent(feedback::setDescription);
        input.getAuthorId().ifPresent(feedback::setAuthorId);
        input.getAuthorEmail().ifPresent(feedback::setAuthorEmail);
        input.getLocation().ifPresent(feedback::setLocation);
        input.getLocationTitle().ifPresent(feedback::setLocationTitle);
        input.getLocale().ifPresent(feedback::setLocale);
        input.getFrontendVersion().ifPresent(feedback::setFrontendVersion);
        input.getBackendVersion().ifPresent(feedback::setBackendVersion);
        input.getBrowser().ifPresent(feedback::setBrowser);
        input.getOs().ifPresent(feedback::setOs);
        input.getPlatform().ifPresent(feedback::setPlatform);
        input.getScreenResolution().ifPresent(feedback::setScreenResolution);
        input.getDevicePixelRatio().ifPresent(feedback::setDevicePixelRatio);
        input.getDisplaySize().ifPresent(feedback::setDisplaySize);

        feedback.setExtra(input.getExtra());

        feedbackRepository.persist(feedback);
    }

    @Override
    public void validateAction(Action action) {
        // rien à valider
    }

}
