#!/bin/bash
DB=feebaco
IP=`./.get_db_ip.sh ${DB}`
docker run -it -e PGPASSWORD=whatever postgres:13 \
  pg_dump -h ${IP} -p 5432 \
  --schema-only --no-owner --exclude-table=flyway_schema_history \
  -U postgres ${DB} > src/main/resources/db/migration/V0.0.0__init_schema.sql
