#!/bin/bash
DB=$1
if [ "${DB}" = "" ]; then
   DB=feebaco
fi
IP=`docker inspect --format '{{ .NetworkSettings.IPAddress }}' postgres-13-${DB}`
echo ${IP}
