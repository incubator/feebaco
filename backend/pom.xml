<?xml version="1.0"?>
<!--
  #%L
  Feebaco::REST
  %%
  Copyright (C) 2018 - 2020 Chorem
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<project xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd" xmlns="http://maven.apache.org/POM/4.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>org.chorem</groupId>
    <artifactId>feebaco</artifactId>
    <version>0.1-SNAPSHOT</version>
  </parent>

  <artifactId>feebaco-backend</artifactId>
  <name>Feebaco::Backend</name>

  <properties>
  </properties>

  <dependencies>
    <dependency>
      <groupId>io.quarkus</groupId>
      <artifactId>quarkus-resteasy</artifactId>
    </dependency>
    <dependency>
      <groupId>io.quarkus</groupId>
      <artifactId>quarkus-undertow</artifactId>
      <scope>runtime</scope>
    </dependency>
    <dependency>
      <groupId>io.quarkus</groupId>
      <artifactId>quarkus-resteasy-jackson</artifactId>
    </dependency>
    <dependency>
      <groupId>com.fasterxml.jackson.datatype</groupId>
      <artifactId>jackson-datatype-guava</artifactId>
    </dependency>

    <dependency>
      <groupId>io.quarkus</groupId>
      <artifactId>quarkus-agroal</artifactId>
    </dependency>
    <dependency>
      <groupId>io.agroal</groupId>
      <artifactId>agroal-api</artifactId>
    </dependency>
    <!-- Hibernate ORM specific dependencies -->
    <dependency>
      <groupId>io.quarkus</groupId>
      <artifactId>quarkus-hibernate-orm</artifactId>
    </dependency>
    <dependency>
      <groupId>io.quarkus</groupId>
      <artifactId>quarkus-hibernate-orm-panache</artifactId>
    </dependency>

    <!-- JDBC driver dependencies -->
    <dependency>
      <groupId>io.quarkus</groupId>
      <artifactId>quarkus-jdbc-postgresql</artifactId>
    </dependency>
    <dependency>
      <groupId>io.quarkus</groupId>
      <artifactId>quarkus-junit5</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>io.rest-assured</groupId>
      <artifactId>rest-assured</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>io.rest-assured</groupId>
      <artifactId>json-path</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>com.google.guava</groupId>
      <artifactId>guava</artifactId>
    </dependency>

    <dependency>
      <groupId>org.immutables</groupId>
      <artifactId>value</artifactId>
    </dependency>

    <dependency>
      <groupId>io.quarkus</groupId>
      <artifactId>quarkus-flyway</artifactId>
    </dependency>

    <dependency>
      <groupId>javax.mail</groupId>
      <artifactId>mail</artifactId>
    </dependency>

    <dependency>
      <groupId>org.nuiton</groupId>
      <artifactId>nuiton-utils</artifactId>
    </dependency>

    <dependency>
      <groupId>org.testcontainers</groupId>
      <artifactId>testcontainers</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.testcontainers</groupId>
      <artifactId>postgresql</artifactId>
      <scope>test</scope>
    </dependency>

    <dependency>
      <groupId>com.vladmihalcea</groupId>
      <artifactId>hibernate-types-52</artifactId>
    </dependency>

    <dependency>
      <groupId>org.openjdk.nashorn</groupId>
      <artifactId>nashorn-core</artifactId>
    </dependency>

    <dependency>
      <groupId>commons-validator</groupId>
      <artifactId>commons-validator</artifactId>
    </dependency>

    <dependency>
      <groupId>com.github.spullara.mustache.java</groupId>
      <artifactId>compiler</artifactId>
    </dependency>

  </dependencies>
  <build>

    <resources>
      <resource>
        <directory>src/main/resources</directory>
        <includes>
          <include>application.properties</include>
        </includes>
        <filtering>true</filtering>
      </resource>
      <resource>
        <directory>src/main/resources</directory>
        <excludes>
          <exclude>application.properties</exclude>
        </excludes>
      </resource>
    </resources>

    <plugins>

      <plugin>
        <artifactId>maven-resources-plugin</artifactId>
        <version>${resourcesPluginVersion}</version>
        <configuration>
          <delimiters>
            <delimiter>%(*)</delimiter>
          </delimiters>
        </configuration>
      </plugin>

      <plugin>
        <groupId>pl.project13.maven</groupId>
        <artifactId>git-commit-id-plugin</artifactId>
        <executions>
          <execution>
            <id>get-the-git-infos</id>
            <goals>
              <goal>revision</goal>
            </goals>
            <phase>initialize</phase>
          </execution>
        </executions>
        <configuration>
          <verbose>false</verbose>
          <includeOnlyProperties>
            <includeOnlyProperty>^git.commit.id.describe$</includeOnlyProperty>
          </includeOnlyProperties>
          <commitIdGenerationMode>full</commitIdGenerationMode>
        </configuration>
      </plugin>

      <plugin>
        <groupId>io.quarkus</groupId>
        <artifactId>quarkus-maven-plugin</artifactId>
        <version>${quarkusVersion}</version>
        <executions>
          <execution>
            <goals>
              <goal>build</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>${surefirePluginVersion}</version>
        <configuration>
          <systemProperties>
            <java.util.logging.manager>org.jboss.logmanager.LogManager</java.util.logging.manager>
          </systemProperties>
        </configuration>
      </plugin>
    </plugins>

    <pluginManagement>
      <plugins>
<!--        <plugin>-->
<!--          <artifactId>maven-dependency-plugin</artifactId>-->
<!--          <version>${dependencyPluginVersion}</version>-->
<!--          <configuration>-->
<!--            <ignoredUnusedDeclaredDependencies>-->
<!--              <ignoredUnusedDeclaredDependency>io.quarkus:quarkus-resteasy</ignoredUnusedDeclaredDependency>-->
<!--              <ignoredUnusedDeclaredDependency>io.quarkus:quarkus-resteasy-jackson</ignoredUnusedDeclaredDependency>-->
<!--            </ignoredUnusedDeclaredDependencies>-->
<!--          </configuration>-->
<!--        </plugin>-->
      </plugins>
    </pluginManagement>

  </build>
  <profiles>
  </profiles>

</project>
