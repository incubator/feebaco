# Feebaco

Feebaco stands for `Fee`d`ba`ck `Co`llector

## Lancement de l'application :

démarrage de maildev : 

* `docker run -d -p 41080:80 -p 41025:25 --name maildev --rm djfarrelly/maildev`

démarrage de postgresql : 

* `docker run --rm --name feebaco-pg10 -e POSTGRES_PASSWORD=toto -d -p 15432:5432 postgres:latest`

dans un terminal, accéder au répertoire de feebaco et l'exécuter avec : 

* `mvn tomcat7:run`

L'UI est ensuite disponible sur http://localhost:8084/feebaco-web/

Pour vérifier que tout va bien : http://localhost:8084/feebaco-web/api/v1/status

Les mails envoyés par l'application sont interceptés par maildev et sont accessibles sur http://localhost:41080/

## Il est possible de travailler sur la partie JS uniquement avec 

* `npm install`
* `npm build`

## Fonctionnement de l'application

Le fichier de configuration de l'application est [feebaco.properties](feebaco-web/src/main/resources/feebaco.properties).

Les routes de l'API sont définies dans le fichier [FeedbacksService](feebaco-web/src/main/java/org/chorem/feebaco/web/rest/FeedbacksService.java).
Ce fichier contient également les méthodes qui permettent de gérer les actions à effectuer à la réception d'un feedback (comme l'envoi de mail).

Le fichier contenant les requêtes à la base de données est [DatamartDao](feebaco/web/database/DatamartDao.java).

Le fichier définissant le front-end de Feebaco est [Feebaco.vue](feebaco-web/src/main/js/vues/Feebaco.vue).

Les scripts clients sont contenus dans le dossier [feebaco_client](feebaco-web/src/main/js/feebaco_client).

Les scripts de migration de la base de données (flyway) sont contenus dans le dossier [migration](feebaco-web/src/main/resources/db/migration)

les méthodes permettant de déclencher ces scripts de migration grâce à flyway sont appelées dans le fichier [FeebacoApplication](feebaco-web/src/main/java/org/chorem/feebaco/web/FeebacoApplication.java)
