/*-
 * #%L
 * Feebaco::Web
 * %%
 * Copyright (C) 2018 - 2020 Chorem
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import Vue from 'vue'
import VueRouter from 'vue-router'
import VueToasted from 'vue-toasted';
import Feebaco from './vues/Feebaco.vue'

Vue.use(VueRouter)
Vue.use(VueToasted, {
    iconPack : 'fontawesome'
})

import 'font-awesome/css/font-awesome.css';

const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: [
    { path: '/', component: Feebaco }
  ]
})

new Vue({
  router,
  el: '#feebaco',
  template: '<Feebaco/>',
  components: { Feebaco }
})
