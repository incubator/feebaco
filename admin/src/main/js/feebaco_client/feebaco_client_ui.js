/*-
 * #%L
 * Feebaco::Web
 * %%
 * Copyright (C) 2018 - 2020 Chorem
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import {Feebaco} from './feebaco_client.js';

var FeebacoUI = {
  	Feedback: function(){
    	this.category = FeebacoUI.getSelectedRadio('feebacoCategory');
    	this.title = document.getElementById('feebacoTitle').value;
    	this.email = document.getElementById('feebacoEmail').value;
    	this.description = document.getElementById('feebacoDescription').value;
  	},

  buildFormular: function(project_id, feebaco){
    var mainDiv = document.createElement('div');
    mainDiv.id = 'FeebacoForm';
    var categoryDiv = document.createElement('div');
    categoryDiv.id = 'FeebacoCategoryDiv';
    Feebaco.fetchProjectCategories(project_id, feebaco).then(categories => {
    	if(categories.length){
      		categoryDiv.appendChild(document.createTextNode('Category : '));
      		categoryDiv.appendChild(document.createElement('br'));
      		for (var i = 0; i<categories.length; i++){

        		var categoryChoice = document.createElement('input');
        		categoryChoice.type = 'radio';
        		categoryChoice.name = 'feebacoCategory';
        		categoryChoice.value = categories[i];

        		var categoryLabel = document.createElement('label');
        		categoryLabel.for = categories[i];
        		categoryLabel.appendChild(document.createTextNode(categories[i]))

        		categoryDiv.appendChild(categoryChoice);
        		categoryDiv.appendChild(categoryLabel);
        		categoryDiv.appendChild(document.createElement('br'));
      		}
    	}
    })
    mainDiv.appendChild(categoryDiv);
    mainDiv.appendChild(document.createTextNode('Title : '));
    mainDiv.appendChild(document.createElement('br'));

    var titleField = document.createElement('input');
    titleField.type = 'text';
    titleField.id = 'feebacoTitle';
    titleField.name = 'title';

    mainDiv.appendChild(titleField);
    mainDiv.appendChild(document.createElement('br'));
    mainDiv.appendChild(document.createTextNode('Email : '));
    mainDiv.appendChild(document.createElement('br'));

    var emailField = document.createElement('input');
    emailField.type = 'text';
    emailField.id = 'feebacoEmail';
    emailField.name = 'email';

    mainDiv.appendChild(emailField);
    mainDiv.appendChild(document.createElement('br'));
    mainDiv.appendChild(document.createTextNode('Description'));
    mainDiv.appendChild(document.createElement('br'));

    var descriptionField = document.createElement('textarea');
    descriptionField.name = 'description';
    descriptionField.id = 'feebacoDescription';
    descriptionField.rows = '10';
    descriptionField.cols = '50';

    mainDiv.appendChild(descriptionField);
    mainDiv.appendChild(document.createElement('br'));

    var sendFeedbackButton = document.createElement('button');
    sendFeedbackButton.id = 'feebacoSubmit'
    sendFeedbackButton.appendChild(document.createTextNode('Envoyer un Feedback'));
    sendFeedbackButton.addEventListener("click", function(){Feebaco.sendFeedback(project_id, new FeebacoUI.Feedback(), feebaco)});

    mainDiv.appendChild(sendFeedbackButton);

    mainDiv.appendChild(document.createElement('br'));

    document.getElementById('feebaco').appendChild(mainDiv);
  },

  getSelectedRadio: function(name){
    var radios = document.getElementsByName(name);
    var selectedRadio = 'none';
    for(var i = 0; i < radios.length; i++){
      if(radios[i].checked){
        selectedRadio = radios[i];
      }
    }
    return selectedRadio.value;
  }

}
export{FeebacoUI}
