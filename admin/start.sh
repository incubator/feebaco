#!/bin/bash
npm run build
docker build -t chorem/feebaco-web -f src/main/docker/Dockerfile .
docker run --rm -it -p 8084:80 --name feebaco-web --volume ${PWD}/src/main/webapp:/usr/share/nginx/html chorem/feebaco-web

